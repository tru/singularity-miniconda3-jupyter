## installing a jupyter minimal environment in singularityCE/apptainer

- Singularity_from_docker: CI produced registry image at registry-gitlab.pasteur.fr/tru/docker-miniconda3-jupyter reused to produce singularity container (OCI)
`singularity build Singularity_from_docker.sif Singularity_from_docker` (might need `sudo`)

- Singularity: complete Singularity recipe for CI
`singularity build Singularity.sif Singularity` (might need `sudo`)

## singularity conversion if you have singularityCE or apptainer installed
```
singularity build docker-miniconda3-jupyter.sif  oras://registry-gitlab.pasteur.fr/tru/singularity-miniconda3-jupyter:latest
singularity exec docker-miniconda3-jupyter.sif jupyter-lab  --no-browser
```

## on Maestro.pasteur.fr
```
module add apptainer
apptainer build docker-miniconda3-jupyter.sif  oras://registry-gitlab.pasteur.fr/tru/singularity-miniconda3-jupyter:latest
apptainer exec -B /pasteur docker-miniconda3-jupyter.sif jupyter-lab  --no-browser
```

## from cache only
```
singularity exec oras://registry-gitlab.pasteur.fr/tru/singularity-miniconda3-jupyter:latest jupyter-lab  --no-browser
```
